package com.mukul.meetingscheduler.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mukul.meetingscheduler.R;
import com.mukul.meetingscheduler.databinding.CellRecyclerBinding;
import com.mukul.meetingscheduler.modals.MeetingModel;

import java.util.ArrayList;
import java.util.List;

public class MeetingRecyclerAdapter extends RecyclerView.Adapter<MeetingRecyclerAdapter.Holder> {

    private List<MeetingModel> list=new ArrayList<>();
    @NonNull
    @Override
    public MeetingRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CellRecyclerBinding binding= DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.cell_recycler,parent,false );
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MeetingRecyclerAdapter.Holder holder, int position) {
        holder.binding.setMeetingModal(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private CellRecyclerBinding binding;

        Holder(CellRecyclerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void submitList(List<MeetingModel> meetingModelList,boolean clearPreviousItems){
        if (clearPreviousItems)
            list.clear();

        list.addAll(meetingModelList);
        notifyDataSetChanged();
    }
}
