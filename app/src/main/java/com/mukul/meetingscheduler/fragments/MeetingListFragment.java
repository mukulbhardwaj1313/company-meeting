package com.mukul.meetingscheduler.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.mukul.meetingscheduler.R;
import com.mukul.meetingscheduler.adapters.MeetingRecyclerAdapter;
import com.mukul.meetingscheduler.databinding.FragmentMeetingListBinding;
import com.mukul.meetingscheduler.viewModels.MeetingListViewModel;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


public class MeetingListFragment extends Fragment {

    private Context context;
    private MeetingListViewModel viewModel;
    public MeetingRecyclerAdapter adapter;



    private final Calendar c = Calendar.getInstance();
    private final int mYear = c.get(Calendar.YEAR);
    private final int mMonth = c.get(Calendar.MONTH);
    private final int mDay = c.get(Calendar.DAY_OF_MONTH);

    public MeetingListFragment() {
    }




    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        viewModel=new ViewModelProvider(this).get(MeetingListViewModel.class);
        FragmentMeetingListBinding binding= DataBindingUtil.inflate(inflater, R.layout.fragment_meeting_list,container,false);
        adapter=new MeetingRecyclerAdapter();

        binding.setLifecycleOwner(this);
        binding.setFragment(this);
        binding.setViewModel(viewModel);
        observeList();
        return binding.getRoot();
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =  context;

    }


    public void selectDate(){

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) ->{},
                mYear, mMonth, mDay);
        datePickerDialog.setOnDateSetListener(new DateSetListener() );
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void showNextDate(){
        c.setTime(Objects.requireNonNull(viewModel.selectedDateLiveData.getValue()));
        c.add(Calendar.DATE, 1);
        Date selectedDate=c.getTime();
        viewModel.getDataForDate(selectedDate);

    }

    public void showPreviousDate(){
        c.setTime(Objects.requireNonNull(viewModel.selectedDateLiveData.getValue()));
        c.add(Calendar.DATE, -1);
        Date selectedDate=c.getTime();
        viewModel.getDataForDate(selectedDate);

    }

    public void bookNewMeeting(){
        NavController navController = Navigation.findNavController((Activity) context, R.id.nav_host_fragment_container);
        navController.navigate(R.id.action_meetingListFragment_to_scheduleNewMeetingFragment);


    }

    private void observeList(){
        viewModel.dataList.observe(getViewLifecycleOwner(),
                meetingModelList -> adapter.submitList(meetingModelList,true)
        );
    }

    private class DateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {


            c.setTimeInMillis(0);
            c.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
            Date selectedDate = c.getTime();
            viewModel.getDataForDate(selectedDate);

        }
    }

}
