package com.mukul.meetingscheduler.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mukul.meetingscheduler.R;
import com.mukul.meetingscheduler.databinding.FragmentScheduleNewMeetingBinding;


public class ScheduleNewMeetingFragment extends Fragment {

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentScheduleNewMeetingBinding binding= DataBindingUtil
                .inflate(inflater,R.layout.fragment_schedule_new_meeting, container, false);
        binding.setFragment(this);
        binding.setLifecycleOwner(this);
        return  binding.getRoot();
    }
    public void bookNewMeeting(){

    }

    @Override
    public void onAttach(@NonNull Context context) {
        this.context = context;
        super.onAttach(context);

    }

    public void onBackPress(){
        ((Activity)context).onBackPressed();
    }
}
