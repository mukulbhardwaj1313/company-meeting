package com.mukul.meetingscheduler;

import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BindingAdapters {

    @BindingAdapter("app:setBooleanVisibility")
    public static void setBooleanVisibility(View view, boolean visibility){

        view.setVisibility(visibility?View.VISIBLE:View.GONE);
    }

    @BindingAdapter("app:setAdapter")
    public static void bindRecyclerViewAdapter(RecyclerView recyclerView, RecyclerView.Adapter<?> adapter) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(adapter);
    }


}
