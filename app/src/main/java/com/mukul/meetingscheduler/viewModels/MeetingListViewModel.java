package com.mukul.meetingscheduler.viewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mukul.meetingscheduler.apiCalls.Configs;
import com.mukul.meetingscheduler.modals.MeetingModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mukul.meetingscheduler.apiCalls.Configs.BASE_URL;

public class MeetingListViewModel extends AndroidViewModel {


    public MutableLiveData<Boolean> PreviousButtonVisibility=new MutableLiveData<>(false);
    private Configs configs = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Configs.class);


    public MutableLiveData<List<MeetingModel>> dataList=new MutableLiveData<>();
    public MutableLiveData<String> selectedDateStringLiveData=new MutableLiveData<>();
    public MutableLiveData<Date> selectedDateLiveData=new MutableLiveData<>();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());

    public MeetingListViewModel(@NonNull Application application) {
        super(application);
        getDataForDate(Calendar.getInstance().getTime());  //hit API with current dat on start of fragment
    }

    private static final String TAG = "MeetingListViewModel";
    public void getDataForDate(Date date) {

        if (date.compareTo(Calendar.getInstance().getTime())>0){
            PreviousButtonVisibility.setValue(true);
        }else {
            PreviousButtonVisibility.setValue(false);

        }

        selectedDateLiveData.setValue(date);
        selectedDateStringLiveData.setValue(dateFormat.format(date));
        Log.e(TAG, "getDataForDate: "+dateFormat.format(date) );
        configs.meetingModelCall(dateFormat.format(date)).enqueue(new Callback<List<MeetingModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<MeetingModel>> call, @NonNull Response<List<MeetingModel>> response) {

                if (response.isSuccessful()){
                    assert response.body() != null;
                    Log.e(TAG, "onResponse: "+response.body() );
                    Collections.sort(response.body(),new SortByDate());
                    Log.e(TAG, "onResponse: "+response.body() );
                    dataList.setValue(response.body());
                }else {
                    dataList.setValue(new ArrayList<>());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<MeetingModel>> call, @NonNull Throwable t) {

            }
        });

    }




    class SortByDate implements Comparator<MeetingModel>
    {


        @Override
        public int compare(MeetingModel m1, MeetingModel m2) {
            try {
                Date d1=timeFormat.parse(m1.getStartTime());
                Date d2=timeFormat.parse(m2.getStartTime());

                if (d1==d2){
                    return 0;
                }
                assert d1 != null;
                if (d1.after(d2)){
                    return 1;
                }

                if (d1.before(d2)){
                    return -1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }
    }
}
