package com.mukul.meetingscheduler.apiCalls;

import com.mukul.meetingscheduler.modals.MeetingModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Configs {

    String BASE_URL = "https://fathomless-shelf-5846.herokuapp.com/api/";


    @GET("schedule")
    Call<List<MeetingModel>> meetingModelCall(@Query("date") String date);
}
